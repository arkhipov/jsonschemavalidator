//
//  SVJsonArrayTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/22/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonArrayTests.h"
#import "SVJsonSchema.h"

@implementation SVJsonArrayTests

-(void)testItemsTypes
{
    SVArray* array = [SVType array];
    
    STAssertNoThrow( [array items:[SVType number]], nil);
    
    id items = @[[SVType string], [SVType null]];
    STAssertNoThrow( [array items:items], nil);
}

-(void)testWrongItemsTypes
{
    SVArray* array = [SVType array];
    
    STAssertThrows( [array items:[NSObject new]], nil);
    
    id items = @[[NSObject new], [NSObject new]];
    STAssertThrows( [array items:items], nil);
    
    items = @[[SVType string], [NSObject new]];
    STAssertThrows( [array items:items], nil);
}

-(void)testSingleItem
{
    SVArray* array = [[SVType array] items:[SVType string]];
    
    NSError* error = nil;
    STAssertNotNil( [array validateJson:@[] error:&error], nil);
    STAssertNil(error, nil);
    
    error = nil;
    NSArray* oneString = @[@"one string in array"];
    STAssertNotNil( [array validateJson:oneString error:&error], nil);
    STAssertNil( error, nil);
    
    error = nil;
    NSArray* strings = @[@"one, two"];
    STAssertNotNil( [array validateJson:strings error:&error], nil);
    STAssertNil( error, nil);
}

-(void)testWrongItemTypes
{
    SVArray* array = [[SVType array] items:[SVType string]];
    
    NSError* error = nil;
    STAssertNotNil([array validateJson:@[@YES] error:&error], nil);
    STAssertNotNil(error, nil);   
}

-(void)testItemTuples
{
    SVArray* array = [[SVType array] items:@[[SVType number], [SVType string]]];

    NSError* error = nil;
    
    id tuple = @[@(100500), @"100500"];
    STAssertNotNil( [array validateJson:tuple error:&error], nil);
    STAssertNil(error, nil);
}

-(void)testItemWrongTuples
{
    SVArray* array = [[SVType array] items:@[[SVType number], [SVType string]]];
    
    NSError* error = nil;
    
    id tuple = @[@(100500), @(100500)];
    STAssertNil( [array validateJson:tuple error:&error], nil);
    STAssertNotNil(error, nil);
}

-(void)testMinItems
{
    SVArray* array = [[[SVType array] items:[SVType string]] minItems:@(1)];
    
    NSError* error = nil;
    STAssertNotNil( [array validateJson:@[@"qwerty"] error:&error], nil);
    STAssertNil( error, nil );
}

-(void)testMinItemsFail
{
    SVArray* array = [[[SVType array] items:[SVType string]] minItems:@(1)];
    
    NSError* error = nil;
    STAssertNil( [array validateJson:@[] error:&error], nil);
    STAssertNotNil( error, nil );
}

-(void)testMaxItems
{
    SVArray* array = [[[SVType array] items:[SVType string]] maxItems:@(1)];
    
    NSError* error = nil;
    STAssertNotNil( [array validateJson:@[@"qwerty"] error:&error], nil);
    STAssertNil( error, nil );
}

-(void)testMaxItemsFail
{
    SVArray* array = [[[SVType array] items:[SVType string]] maxItems:@(0)];
    
    NSError* error = nil;
    STAssertNil( [array validateJson:@[@"1"] error:&error], nil);
    STAssertNotNil( error, nil );
}

-(void)testUniqueItems
{
    SVArray* array = [[[SVType array] items:[SVType string]] uniqueItems:YES];
    
    NSError* error = nil;
    id notUniqueItems = @[@"1", @"2", @"3"];
    
    STAssertNotNil( [array validateJson:notUniqueItems error:&error], nil);
    STAssertNil( error, nil );
}

-(void)testUniqueItemsFails
{
    SVArray* array = [[[SVType array] items:[SVType string]] uniqueItems:YES];

    NSError* error = nil;
    id notUniqueItems = @[@"qwerty", @"qwerty", @"qwerty"];

    STAssertNil( [array validateJson:notUniqueItems error:&error], nil);
    STAssertNotNil( error, nil );
}

/** Array schema must fail when there's enough items but not enough _valid_ items. */
-(void)testMinItemsWithFailures
{
    SVArray* arraySchema = [[[SVType array] items:[SVType string]] minItems:@1];
    
    NSError *error = nil;
    NSArray *items = @[@1, @2, @3];
    STAssertNil([arraySchema validateJson:items error:&error], nil);
    STAssertNotNil(error, nil);
}

@end
