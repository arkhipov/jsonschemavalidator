//
//  SVJsonSchemaCreationTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/23/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonSchemaCreationTests.h"
#import "SVJsonSchema.h"
#import "SVType.h"

@implementation SVJsonSchemaCreationTests

-(void)testCreateWrongType
{
    STAssertNil( [SVType schemaWithDictionary:nil], nil);
    STAssertNil( [SVType schemaWithDictionary:@{}], nil);
    STAssertNil( [SVType schemaWithDictionary:@{SVTypeKey: @"some ugly type"}], nil);
}

-(void)testCreateString
{
    id type = [SVType schemaWithDictionary:@{SVTypeKey: SVStringKey}];
    
    STAssertNotNil(type, nil);
    STAssertEqualObjects([type class], [SVString class], nil);
}

-(void)testCreateArray
{
    id type = [SVType schemaWithDictionary:@{SVTypeKey: SVArrayKey}];
    
    STAssertNotNil(type, nil);
    STAssertEqualObjects([type class], [SVArray class], nil);
}

-(void)testCreateObject
{
    id type = [SVType schemaWithDictionary:@{SVTypeKey: SVObjectKey}];
    
    STAssertNotNil(type, nil);
    STAssertEqualObjects([type class], [SVObject class], nil);
}

-(void)testCreateInteger
{
    id type = [SVType schemaWithDictionary:@{SVTypeKey: SVIntegerKey}];
    
    STAssertNotNil(type, nil);
    STAssertEqualObjects([type class], [SVInteger class], nil);
}

-(void)testCreateNumber
{
    id type = [SVType schemaWithDictionary:@{SVTypeKey: SVNumberKey}];
    
    STAssertNotNil(type, nil);
    STAssertEqualObjects([type class], [SVNumber class], nil);
}

-(void)testCreateNumberWithMinimum
{
    SVNumber* type = [SVType schemaWithDictionary:@{SVTypeKey: SVNumberKey, SVMinimumKey : @(0)}];
    STAssertNotNil( type, nil);
    STAssertNotNil( type.minimum, nil);
}

-(void)testCreateNumberWithMaximum
{
    SVNumber* type = [SVType schemaWithDictionary:@{SVTypeKey: SVNumberKey, SVMaximumKey : @(0)}];
    STAssertNotNil( type, nil);
    STAssertNotNil( type.maximum, nil);
}

-(void)testCreateNumberWithWrongMinimum
{
    SVNumber* type = [SVType schemaWithDictionary:@{SVTypeKey: SVNumberKey, SVMinimumKey : @""}];
    STAssertNotNil( type, nil);
    STAssertNil( type.minimum, nil);
}

-(void)testCreateNumberWithWrongMaximum
{
    SVNumber* type = [SVType schemaWithDictionary:@{SVTypeKey: SVNumberKey, SVMaximumKey : @""}];
    STAssertNotNil( type, nil);
    STAssertNil( type.maximum, nil);
}


-(void)testCreateNumberWithExclusiveMinimum
{
    SVNumber* type = [SVType schemaWithDictionary:@{SVTypeKey: SVNumberKey, SVExclusiveMinimumKey : @YES }];
    STAssertNotNil( type, nil);
    STAssertTrue( type.exclusiveMinimum, nil);
}

-(void)testCreateNumberWithExclusiveMaximum
{
    SVNumber* type = [SVType schemaWithDictionary:@{SVTypeKey: SVNumberKey, SVExclusiveMaximumKey : @YES}];
    STAssertNotNil( type, nil);
    STAssertTrue( type.exclusiveMaximum, nil);
}

-(void)testCreateNull
{
    id type = [SVType schemaWithDictionary:@{SVTypeKey: SVNullKey}];
    
    STAssertNotNil(type, nil);
    STAssertEqualObjects([type class], [SVNull class], nil);
}

-(void)testCreateBoolean
{
    id type = [SVType schemaWithDictionary:@{SVTypeKey: SVBooleanKey}];
    
    STAssertNotNil(type, nil);
    STAssertEqualObjects([type class], [SVBoolean class], nil);
}


@end
