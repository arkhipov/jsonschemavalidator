//
//  SVJsonSchemaParentLinkTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 03.06.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonSchemaParentLinkTests.h"
#import "SVJsonSchema.h"

@implementation SVJsonSchemaParentLinkTests

-(void)testParentForProperties
{
    SVObject* object = [SVType object];
    STAssertEqualObjects(object.parent, nil, nil);
    
    SVNull* null = [SVType null];
    STAssertEqualObjects(null.parent, nil, nil);
    
    [object properties:@{@"key":null}];
    
    STAssertEqualObjects(null.parent, object, nil);
    
    [object properties:@{}];
    STAssertEqualObjects(null.parent, nil, nil);
}

-(void)testParentForItems
{
    SVArray* array = [SVType array];
    STAssertEqualObjects(array.parent, nil, nil);
    
    SVNumber* number = [SVType number];
    STAssertEqualObjects(number.parent, nil, nil);
    
    [array items:number];
    STAssertEqualObjects(number.parent, array, nil);
    
    [array items:nil];
    STAssertEqualObjects(number.parent, nil, nil);
}

@end
