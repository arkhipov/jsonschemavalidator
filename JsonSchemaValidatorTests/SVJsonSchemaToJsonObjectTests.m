//
//  SVJsonSchemaToJsonObjectTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 24.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonSchemaToJsonObjectTests.h"
#import "SVJsonSchema.h"

@interface SVTestClassToJson : NSObject;

@end

@implementation SVTestClassToJson

@end

@implementation SVJsonSchemaToJsonObjectTests

-(void)testType
{
    id enumVal = @[@1, @2];
    id type = [[[[[[[[SVType new] schema:@"schema"]
                    schemaId:@"schemaId"]
                   title:@"title"]
                  description:@"schemaDescription"]
                 defaultVal:@"defaultVal"]
                required:YES]
               enumValues:enumVal];

    id json = [type toJsonObject];

    STAssertEqualObjects( json[SVSchemaKey], @"schema", nil);
    STAssertEqualObjects( json[SVIDKey], @"schemaId", nil);
    STAssertEqualObjects( json[SVDescriptionKey], @"schemaDescription", nil);
    STAssertEqualObjects( json[SVDefaultKey], @"defaultVal", nil);
    STAssertEqualObjects( json[SVRequiredKey], @YES, nil);
    STAssertEqualObjects( json[SVEnumKey], enumVal, nil);
    
    STAssertNil( json[SVObjcClassKey], nil);
}

-(void)testString
{
    id json = [[[SVType string] format:SVURLKey] toJsonObject];
    STAssertEqualObjects(json[SVTypeKey], SVStringKey, nil);
    STAssertEqualObjects(json[SVFormatKey], SVURLKey, nil);
}

-(void)testNumber
{
    id json = [[[[[[SVType number] minimum:@0] exclusiveMinimum:YES] maximum:@10] exclusiveMaximum:YES] toJsonObject];
    
    STAssertEqualObjects(json[SVTypeKey], SVNumberKey, nil);
    STAssertEqualObjects(json[SVMinimumKey], @0, nil);
    STAssertEqualObjects(json[SVExclusiveMinimumKey], @YES, nil);
    STAssertEqualObjects(json[SVMaximumKey], @10, nil);
    STAssertEqualObjects(json[SVExclusiveMaximumKey], @YES, nil);
    STAssertNil(json[SVObjcClassKey], nil);
}

-(void)testNull
{
    id json = [[SVType null] toJsonObject];
    STAssertEqualObjects(json[SVTypeKey], SVNullKey, nil);
    STAssertNil(json[SVObjcClassKey], nil);
}

-(void)testArray
{
    id json = [[[[[SVType array] minItems:@1] maxItems:@2] uniqueItems:YES] toJsonObject];
    
    STAssertEqualObjects(json[SVTypeKey], SVArrayKey, nil);
    STAssertEqualObjects(json[SVUniqueItemsKey], @YES, nil);
    STAssertEqualObjects(json[SVMinItemsKey], @1, nil);
    STAssertEqualObjects(json[SVMaxItemsKey], @2, nil);
}

-(void)testArrayItem
{
    id json = [[[SVType array] items:[SVTestClassToJson jsonSchema]] toJsonObject];
    STAssertEqualObjects(json[SVItemsKey], [[SVTestClassToJson jsonSchema] toJsonObject], nil);
}

-(void)testArrayItemFormInstance
{
    id json = [[@[[SVTestClassToJson class]] jsonSchema] toJsonObject];
    STAssertEqualObjects(json[SVItemsKey], [[SVTestClassToJson jsonSchema] toJsonObject], nil);
}

-(void)testArrayTuple
{
    id tuple = [@[@1, @1] jsonSchema];
    id json = [[[SVType array] items:tuple] toJsonObject];
    STAssertEqualObjects(json[SVItemsKey], [tuple toJsonObject], nil);
}

-(void)testObject
{
    id json = [[@{} jsonSchema] toJsonObject];
    STAssertEqualObjects(json[SVTypeKey], SVObjectKey, nil);
    STAssertNil(json[SVObjcClassKey], nil);
}

-(void)testCustomObject
{
    id json = [[SVTestClassToJson jsonSchema] toJsonObject];
    STAssertEqualObjects(json[SVTypeKey], SVObjectKey, nil);
    STAssertEqualObjects(json[SVObjcClassKey], NSStringFromClass([SVTestClassToJson class]), nil);
}

@end
