//
//  SVJsonStringURIFormatTests.m
//  JsonSchemaValidator
//
//  Created by Gleb Arkhipov on 06.07.14.
//  Copyright (c) 2014 Max Lunin. All rights reserved.
//

#import "SVJsonSchema.h"

#import <SenTestingKit/SenTestingKit.h>

@interface SVJsonStringURIFormatTests : SenTestCase

@property (nonatomic, strong) SVType *uriType;

@end

@implementation SVJsonStringURIFormatTests

- (void)setUp
{
    [super setUp];
    self.uriType = [[SVType string] format:@"uri"];
}

- (void)tearDown
{
    self.uriType = nil;
    [super tearDown];
}

- (void)testExample
{
    NSError *error = nil;
    
    error = nil;
    STAssertNotNil([self.uriType validateJson:@"http://www.google.com/" error:&error], nil);
    STAssertNil(error, nil);
    
    error = nil;
    STAssertNotNil([self.uriType validateJson:@"http://www.google.com?arg=val&arg1=val1#fragment" error:&error], nil);
    STAssertNil(error, nil);
    
    error = nil;
    STAssertNil([self.uriType validateJson:@"google.com" error:&error], nil);
    STAssertNotNil(error, nil);
    
    error = nil;
    STAssertNil([self.uriType validateJson:@"/somepath.ext" error:&error], nil);
    STAssertNotNil(error, nil);
}

@end
