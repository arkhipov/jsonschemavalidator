//
//  SVJsonValidateAndInstanciateTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 24.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonValidateAndInstanciateTests.h"
#import "SVJsonSchema.h"

@interface SVTestPropertyClass : NSObject

@property (strong, nonatomic) NSString* anotherProperty;

@end

@implementation SVTestPropertyClass

@end

@interface SVTestClass : NSObject

@property (strong, nonatomic) NSString* stringProperty;
@property (strong, nonatomic) NSNumber* numberProperty;
@property (strong, nonatomic) NSArray* arrayProperty;
@property (strong, nonatomic) id idProperty;
@property (strong, nonatomic) SVTestPropertyClass* anotherClass;
@property (strong, nonatomic) NSArray* arrayOfNumberProperty;
@property (strong, nonatomic) NSArray* tupleProperty;

@end

@implementation SVTestClass

+(void)prepareJsonSchema:( SVArray* )schema forProperty:(NSString *)property
{
    if ( [property isEqual:@"arrayOfNumberProperty"] )
        schema.items = [SVType number];
    
    if ( [property isEqual:@"tupleProperty"] )
        schema.items = @[[SVType number], [SVType string]];
}

@end

@implementation SVJsonValidateAndInstanciateTests

-(void)testPropertyStringType
{
    SVType* stringProperty = [SVTestClass jsonSchemaForProperty:@"stringProperty"];

    STAssertEqualObjects([stringProperty class], [SVString class], nil);
}

-(void)testPropertyNumberType
{
    SVType* numberProperty = [SVTestClass jsonSchemaForProperty:@"numberProperty"];

    STAssertEqualObjects([numberProperty class], [SVNumber class], nil);
}

-(void)testPropertyArrayType
{
    SVType* arrayProperty = [SVTestClass jsonSchemaForProperty:@"arrayProperty"];
    
    STAssertEqualObjects([arrayProperty class], [SVArray class], nil);
}

-(void)testPropertyArrayOfNumber
{
    SVArray* schema = (SVArray*)[SVTestClass jsonSchemaForProperty:@"arrayOfNumberProperty"];
    
    STAssertEqualObjects([schema.items class], [[SVType number] class], nil);
}

-(void)testPropertyTupleArray
{
    SVArray* schema = (SVArray*)[SVTestClass jsonSchemaForProperty:@"tupleProperty"];
    
    NSError* error = nil;

    id json = @[@1, @"string"];
    id validated = [schema validateJson:json error:&error];
    STAssertEqualObjects( validated, json, nil );
}

-(void)testPropertyTupleArrayWrongTuple
{
    SVArray* schema = (SVArray*)[SVTestClass jsonSchemaForProperty:@"tupleProperty"];
    
    NSError* error = nil;

    id json = @[@1, @1];
    STAssertNil( [schema validateJson:json error:&error], nil );
    STAssertNotNil(error, nil);
}


-(void)testPropertyIDType
{
    SVType* idProperty = [SVTestClass jsonSchemaForProperty:@"idProperty"];
    STAssertEqualObjects([idProperty class], nil, nil);
}

-(void)testPropertyOtherClass
{
    SVObject* classProperty = (SVObject*)[SVTestClass jsonSchemaForProperty:@"anotherClass"];
    STAssertEqualObjects([classProperty class], [SVObject class], nil);
    STAssertEqualObjects(classProperty.objcClass, [SVTestPropertyClass class], nil);
}

-(void)testObjectProperties
{
    SVObject* schema = (SVObject*)[SVTestClass jsonSchema];
    
    STAssertNotNil(schema, nil);
    STAssertNotNil(schema.properties, nil);
    STAssertEqualObjects([schema.properties[@"stringProperty"] class], [SVString class], nil);
}

-(void)testInsensetivePropertyNaming
{
    SVType* schema = [SVTestClass jsonSchema];
    
    NSError* error = nil;
    
    id stringProperty = @"some value";
    id validated = [schema validateJson:@{@"string_property": stringProperty} error:&error];
    SVTestClass* class = [schema instantiateValidatedJson:validated];
    STAssertNotNil(class, nil);
    STAssertEqualObjects(class.stringProperty, stringProperty, nil);
}

-(void)testInsensetivePropertyHardNaming
{
    SVObject* schema = [(SVObject*)[SVTestClass jsonSchema] hardNaming:YES];
    
    NSError* error = nil;
    
    id stringProperty = @"some value";
    id validated = [schema validateJson:@{@"string_property": stringProperty} error:&error];
    SVTestClass* class = [schema instantiateValidatedJson:validated];
    STAssertNotNil(class, nil);
    STAssertNil(class.stringProperty, stringProperty, nil);
}

@end
