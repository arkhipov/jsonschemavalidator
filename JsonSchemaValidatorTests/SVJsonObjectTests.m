//
//  SVJsonValidatorDefaultTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 21.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonObjectTests.h"
#import "SVJsonSchema.h"

@implementation SVJsonObjectTests

-(void)testDefaultValue
{
    NSString* key = @"key";
    NSString* defaultValue = @"qwerty";
    SVType* scheme = [[SVType object] properties:@{ key: [[SVType string] defaultVal:defaultValue]}];
    
    NSError* error = nil;
    NSDictionary* validated = [scheme validateJson:@{} error:&error];
    STAssertNotNil(validated[key], nil);
    STAssertEqualObjects(defaultValue, validated[key], nil);
}

-(void)testPropertiesValidation
{
    id key1 = @"key1";
    id key2 = @"key2";
    SVObject* object = [[SVType object] properties:@{key1:[SVType string],
                                              key2:[SVType number]}];
    NSError* error = nil;
    STAssertNotNil( [object validateJson:@{} error:&error], nil);
    STAssertNil( error, nil);
    
    STAssertNotNil( [object validateJson:@{key1:@"qwerty"} error:&error], nil);
    STAssertNil( error, nil);
    
    STAssertNotNil( [object validateJson:@{key2:@(100500)} error:&error], nil);
    STAssertNil( error, nil);
    
    id obj = @{key1:@"qwerty", key2:@(100500) };
    STAssertNotNil( [object validateJson:obj error:&error], nil);
    STAssertNil( error, nil);
}

-(void)testWrongPropertyTypeValidation
{
    id key = @"key";
    SVObject* object = [[SVType object] properties:@{key:[[SVType number] required:YES]}];
    
    NSError* error = nil;
    NSDictionary* empty = [object validateJson:@{key:@"that property must be number"}
                                         error:&error];
    STAssertNil(empty, nil);
    
    NSUInteger emptyCount = 0;
    STAssertEquals(empty.count, emptyCount, nil);
    STAssertNotNil( error, nil);
}

-(void)testExtraProperties
{
    id key = @"key";
    SVObject* object = [SVType object];
    
    NSError* error = nil;
    NSDictionary* result = [object validateJson:@{key:@"string value"} error:&error];
    STAssertNotNil(result, nil);
    
    NSUInteger resultCount = 1;
    STAssertEquals(result.count, resultCount, nil);
    STAssertNil( error, nil);
}

-(void)testRequiredProperties
{
    id key = @"key";
    SVObject* requiredPropertySchema = [[SVType object] properties:@{key:[[SVType number] required:YES] }];
    
    NSError* error = nil;
    STAssertNil([requiredPropertySchema validateJson:@{}
                                                  error:&error], nil);
    STAssertNotNil( error, nil);
}

/** Array schema must pass when its object has an error in an optional property. */
-(void)testArrayPropertyWithItemsWithFailedOptionals
{
    SVObject *objectSchema = [[SVType object] properties:
                              @{@"array": [[[[SVType array] required:YES] minItems:@1] items:
                                           [[SVType object] properties:
                                            @{@"optionalKey": [SVType string]}]]}];
    
    NSError *error = nil;
    NSDictionary *dict = @{ @"array": @[@{@"optionalKey": @1}] };
    NSDictionary *validDict = [objectSchema validateJson:dict error:&error];
    STAssertNotNil(validDict, nil);
    STAssertNotNil(validDict[@"array"], nil);
    STAssertNotNil(error, nil);
}

-(void)test1
{
    SVObject *objectSchema = [[SVType object] properties:
                              @{@"prop": [[[SVType object] required:YES] enumValues:
                                          @[
                                            [[SVType object] properties:@{@"prop1":[SVType number]}],
                                            [[SVType object] properties:@{@"prop2":[SVType string]}]
                                            ]
                                          ]}];
    
    NSError *error = nil;

    // valid with prop1
    STAssertNotNil([objectSchema validateJson:@{@"prop":@{@"prop1":@1234}} error:&error], nil);
    STAssertNotNil([objectSchema validateJson:@{@"prop":@{@"prop1":@(-0.5)}} error:&error], nil);
    
    // valid with prop2
    STAssertNotNil([objectSchema validateJson:@{@"prop":@{@"prop2":@"qwer"}} error:&error], nil);
    STAssertNotNil([objectSchema validateJson:@{@"prop":@{@"prop2":@""}} error:&error], nil);
    
    // properties are optional, empty dict is ok
    STAssertNotNil([objectSchema validateJson:@{@"prop":@{}} error:&error], nil);

    // invalid prop1/prop2 types are also ok
    STAssertNotNil([objectSchema validateJson:@{@"prop":@{@"prop1":@"string"}} error:&error], nil);
    STAssertNotNil([objectSchema validateJson:@{@"prop":@{@"prop2":@123}} error:&error], nil);
    STAssertNotNil([objectSchema validateJson:@{@"prop":@{@"prop2":[NSNull null]}} error:&error], nil);

    // invalid prop types
    STAssertNil([objectSchema validateJson:@{@"prop":@""} error:&error], nil);
    STAssertNil([objectSchema validateJson:@{@"prop":@"uiop"} error:&error], nil);
    STAssertNil([objectSchema validateJson:@{@"prop":@123} error:&error], nil);
    STAssertNil([objectSchema validateJson:@{@"prop":[NSNull null]} error:&error], nil);
    STAssertNil([objectSchema validateJson:@{@"prop":@[@"qwer"]} error:&error], nil);
}

@end
