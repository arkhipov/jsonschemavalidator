//
//  SVJsonTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 29.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonTests.h"
#import "SVJsonSchema.h"

@implementation SVJsonTests

-(void)testValidate
{
    NSString* jsonString = @"{\"AuthorizedUser\":{ \"ID\":3, \"Name\":\"Тестовый пользователь\"}, \"Success55\":true }";
    NSError* error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                              options:0
                                                error:&error];
    STAssertNotNil(json, nil);
    STAssertNil(error, nil);
    
    id authorizationSchema = [[SVType object] properties:@{
                              @"AuthorizedUser" : [[SVType object] properties:@{
                                                   @"ID": [SVType number],
                                                   @"Name": [SVType string]
                                                   }],
                              @"Success": [SVType boolean]
                              }];

    NSDictionary* validated = [authorizationSchema validateJson:json error:&error];
    STAssertNotNil(validated, nil);
    STAssertNil(error, nil);
    NSUInteger keysCount = 2;
    STAssertEquals(validated.count, keysCount, nil);
}

-(void)testIgnoreInvalidOptional
{
    NSString* jsonString = @"{\"AuthorizedUser\":{ \"ID\":3, \"Name\":\"Тестовый пользователь\"}, \"Success55\":true }";
    NSError* error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                              options:0
                                                error:&error];
    STAssertNotNil(json, nil);
    STAssertNil(error, nil);
    
    id authorizationSchema = [[SVType object] properties:
                              @{
                                @"AuthorizedUser" : [SVType object],
                                @"Success55": [SVType string],
                                @"Success": [SVType boolean]
                                }];
    
    // Success55 is to be dropped as invalid but not required
    NSDictionary* validated = [authorizationSchema validateJson:json error:&error];
    STAssertNotNil(validated, nil);
    STAssertNotNil(error, nil);
    NSUInteger keysCount = 1;
    STAssertEquals(validated.count, keysCount, nil);
}

-(void)testValidateInvalidRequired
{
    NSString* jsonString = @"{\"AuthorizedUser\":{ \"ID\":3, \"Name\":\"Тестовый пользователь\"}, \"Success55\":true }";
    NSError* error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                              options:0
                                                error:&error];
    STAssertNotNil(json, nil);
    STAssertNil(error, nil);
    
    id authorizationSchema = [[SVType object] properties:
                              @{
                                @"AuthorizedUser" : [SVType object],
                                @"Success55": [[SVType string] required:YES],
                                }];
    
    // Success55 is to be dropped as invalid but not required
    NSDictionary* validated = [authorizationSchema validateJson:json error:&error];
    STAssertNil(validated, nil);
    STAssertNotNil(error, nil);
}

-(void)testValidateRequred
{
    NSString* jsonString = @"{\"AuthorizedUser\":{ \"ID\":3, \"Name\":\"Тестовый пользователь\"}, \"Success55\":true }";
    NSError* error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                              options:0
                                                error:&error];
    STAssertNotNil(json, nil);
    STAssertNil(error, nil);
    
    id authorizationSchema = [[SVType object] properties:@{
                              @"AuthorizedUser" : [[SVType object] properties:@{
                                                   @"ID": [[SVType number] required:YES],
                                                   @"Name": [[SVType string] required:YES]
                                                   }],
                              @"Success": [[SVType boolean] required:YES]
                              }];
    
    NSDictionary* validated = [authorizationSchema validateJson:json error:&error];
    STAssertNil(validated, nil);
    STAssertNotNil(error, nil);
}

-(void)testValidateOrNull
{
    NSString* jsonString = @"{\"AuthorizedUser\":{ \"ID\":3, \"Name\":\"Тестовый пользователь\"}, \"Success55\":true }";
    NSError* error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                              options:0
                                                error:&error];
    STAssertNotNil(json, nil);
    STAssertNil(error, nil);
    
    id authorizationSchema = [[SVType object] properties:@{
                              @"AuthorizedUser" : [[SVType object] properties:@{
                                                   @"ID": [[SVType number] required:YES],
                                                   @"Name": [[SVType string] required:YES]
                                                   }],
                              @"Success": [[SVType boolean] required:YES]
                              }];
    
    NSDictionary* validated = [authorizationSchema validateJson:json error:&error];
    STAssertNil(validated, nil);
    STAssertNotNil(error, nil);
}

-(void)testValidateNumberMaximum
{
    NSString* jsonString = @"{\"Diameter\":1992.4, \"Height\":70}";
    NSError* error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
    
    STAssertNotNil(json, nil);
    STAssertNil(error, nil);
    
    id schema = [[SVType object] properties:@{
                 @"Diameter" : [[[[SVType number] minimum:@0.5] maximum:@500] required:YES],
                 @"Height" : [[[SVType number] minimum:@0.1] maximum:@1600]
                 }];

    NSDictionary* validated = [schema validateJson:json error:&error];
    STAssertNil(validated, nil);
    STAssertNotNil(error, nil);
}

@end
