//
//  SVJsonValidatorTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonValidatorTests.h"
#import "SVJsonSchema.h"

@implementation SVJsonValidatorTests

-(void)testStringValidation
{
    SVString* string = [[SVType string] schemaId:@"String"];
    
    NSError* error = nil;
    STAssertNotNil( [string validateJson:@"qwerty" error:&error], nil);
    STAssertNil(error, nil);
    STAssertNil( [string validateJson:@(100500) error:&error], nil);
    STAssertNotNil(error, nil);
    error = nil;
    STAssertNil( [string validateJson:[NSNull null] error:&error], nil);
    STAssertNotNil(error, nil);
}

-(void)testNullValidation
{
    SVNull* null = [SVType null];
    NSError* error = nil;
    STAssertNotNil( [null validateJson:[NSNull null] error:&error], nil);
    STAssertNil( error, nil);
    error = nil;
    STAssertNil( [null validateJson:@(100500) error:&error], nil);
    STAssertNotNil( error, nil);
    error = nil;
    STAssertNil( [null validateJson:@"" error:&error], nil);
    STAssertNotNil( error, nil);
    error = nil;
    STAssertNil( [null validateJson:@{} error:&error], nil);
    STAssertNotNil( error, nil);
    error = nil;
    STAssertNil( [null validateJson:@[] error:&error], nil);
    STAssertNotNil( error, nil);
}

-(void)testObjectValidation
{
    SVObject* object = [SVType object];
    NSError* error = nil;
    STAssertNotNil([object validateJson:@{} error:&error], nil);
    STAssertNil( error, nil);
    error = nil;
    STAssertNil( [object validateJson:@(100500) error:&error], nil);
    STAssertNotNil( error, nil);
    error = nil;
    STAssertNil( [object validateJson:@"" error:&error], nil);
    STAssertNotNil( error, nil);
    error = nil;
    STAssertNil( [object validateJson:[NSNull null] error:&error], nil);
    STAssertNotNil( error, nil);
    error = nil;
    STAssertNil( [object validateJson:@[] error:&error], nil);
    STAssertNotNil( error, nil);
}

-(void)testArrayValidation
{
    SVArray* array = [SVType array];
    NSError* error = nil;
    STAssertNotNil([array validateJson:@[] error:&error], nil);
    STAssertNil( error, nil);
    STAssertNil( [array validateJson:@(100500) error:&error], nil);
    STAssertNotNil(error, nil);
    error = nil;
    STAssertNil( [array validateJson:@"" error:&error], nil);
    STAssertNotNil(error, nil);
    error = nil;
    STAssertNil( [array validateJson:[NSNull null] error:&error], nil);
    STAssertNotNil(error, nil);
    error = nil;
    STAssertNil( [array validateJson:@{} error:&error], nil);
    STAssertNotNil(error, nil);
    error = nil;
}

@end
