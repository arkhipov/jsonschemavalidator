//
//  SVJsonInstanciateTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 24.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonInstanciateTests.h"
#import "SVJsonSchema.h"

@interface SVJsonTestClass1 : NSObject

@property (strong, nonatomic) NSString* stringProperty;
@property (strong, nonatomic) NSNumber* numberProperty;

@end

@implementation SVJsonTestClass1

-(NSString*)description { return [NSString stringWithFormat:@"<%@ '%@' %@>", NSStringFromClass([self class]), self.stringProperty, self.numberProperty]; }

@end

@implementation SVJsonInstanciateTests

-(NSDictionary*)jsonNamed:( NSString* )name
{
    NSString* path = [[NSBundle bundleForClass:[self class]] pathForResource:name ofType:@"json"];
    NSData* data = [NSData dataWithContentsOfFile:path];
    
    NSError* error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    if ( error )
    {
        NSLog(@"--> %@", [error localizedDescription]);
    }

    return json;
}

-(void)testInstanciate
{
    id arrayOfNumbers = @"arrayOfNumbers";
    id arrayKey = @"array";
    id boolKey = @"bool";
    
    SVType* schema = [[SVType object] properties:@{
                  arrayOfNumbers:[@[[SVType number]] jsonSchema],
                        arrayKey:[@[[SVJsonTestClass1 jsonSchema]] jsonSchema],
                         boolKey:[SVType boolean]
                      }];
    
    id json = [self jsonNamed:@"SVJsonInstanciateTests.data1"];
    
    NSError* error = nil;
    id validatedJson = [schema validateJson:json error:&error];
    STAssertNotNil(validatedJson, nil);
    STAssertNil(error, nil);
    
    NSDictionary* instance = [schema instantiateValidatedJson:validatedJson];
    
    STAssertNotNil(instance, nil);
    
    NSArray* array = instance[arrayKey];
    STAssertNotNil(array, nil);
    STAssertNotNil(instance[boolKey], nil);
    
    STAssertTrue([array isJsonArray], nil);
    STAssertTrue(array.count == 4, nil);
    
    for ( SVJsonTestClass1* testObject in array)
    {
        STAssertEqualObjects([testObject class], [SVJsonTestClass1 class], nil);
        STAssertNotNil(testObject.stringProperty, nil);
        STAssertNotNil(testObject.numberProperty, nil);
    }
}

-(void)testValidateAndInstanciate
{
    id schema = [SVType object];
    
    NSError* error = nil;
    STAssertEqualObjects([schema validateAndInstanciateJson:@{} error:&error], @{}, nil);
    STAssertNil(error, nil);
}

@end
