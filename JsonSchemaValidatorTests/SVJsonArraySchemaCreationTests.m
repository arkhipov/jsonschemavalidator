//
//  SVJsonArraySchemaCreationTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/23/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonArraySchemaCreationTests.h"
#import "SVJsonSchema.h"

@implementation SVJsonArraySchemaCreationTests

-(void)testMinItems
{
    SVArray* array = [SVType schemaWithDictionary:@{SVTypeKey: SVArrayKey, SVMinItemsKey: @(2)}];
    
    STAssertNotNil(array, nil);
    STAssertNotNil( array.minItems, nil);
}

-(void)testMinItemsWrongType
{
    SVArray* array = [SVType schemaWithDictionary:@{SVTypeKey: SVArrayKey, SVMinItemsKey: @""}];
    
    STAssertNotNil(array, nil);
    STAssertNil( array.minItems, nil);
}

-(void)testMaxItems
{
    SVArray* array = [SVType schemaWithDictionary:@{SVTypeKey: SVArrayKey, SVMaxItemsKey: @(2)}];
    
    STAssertNotNil(array, nil);
    STAssertNotNil( array.maxItems, nil);
}

-(void)testMaxItemsWrongType
{
    SVArray* array = [SVType schemaWithDictionary:@{SVTypeKey: SVArrayKey, SVMaxItemsKey: @""}];
    
    STAssertNotNil(array, nil);
    STAssertNil( array.maxItems, nil);
}

-(void)testUniqueItems
{
    SVArray* array = [SVType schemaWithDictionary:@{SVTypeKey: SVArrayKey, SVUniqueItemsKey: @YES}];
    STAssertNotNil( array, nil );
    STAssertTrue( array.uniqueItems, nil);
}

-(void)testUniqueItemsWrongType
{
    SVArray* array = [SVType schemaWithDictionary:@{SVTypeKey: SVArrayKey, SVUniqueItemsKey: @(100000)}];
    STAssertNotNil( array, nil );
    STAssertFalse( array.uniqueItems, nil);
}

-(void)testItemsNull
{
    SVArray* array = [SVType schemaWithDictionary:@{SVTypeKey: SVArrayKey, SVItemsKey:[NSNull null] }];
    STAssertNotNil( array, nil );
    STAssertNil( array.items, nil);
}

-(void)testItemsString
{
    SVArray* array = [SVType schemaWithDictionary:@{SVTypeKey: SVArrayKey, SVItemsKey:@{SVTypeKey: SVStringKey} }];
    STAssertNotNil( array, nil );
    STAssertNotNil( array.items, nil);
    STAssertEqualObjects( [array.items class] , [SVString class], nil);
}

-(void)testItemsArrayType
{
    SVArray* array = [SVType schemaWithDictionary:@{SVTypeKey: SVArrayKey, SVItemsKey:@[@{SVTypeKey: SVStringKey}, @{SVTypeKey: SVStringKey}] }];
    STAssertNotNil( array, nil );
    STAssertNotNil( array.items, nil);
    STAssertEqualObjects( [array.items[0] class] , [SVString class], nil);
    STAssertEqualObjects( [array.items[1] class] , [SVString class], nil);
}

@end
