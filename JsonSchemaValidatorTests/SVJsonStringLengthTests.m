//
//  SVJsonStringLengthTests.m
//  JsonSchemaValidator
//
//  Created by Gleb Arkhipov on 03.07.14.
//  Copyright (c) 2014 Max Lunin. All rights reserved.
//

#import "SVJsonSchema.h"

#import <SenTestingKit/SenTestingKit.h>

@interface SVJsonStringLengthTests : SenTestCase

@end

@implementation SVJsonStringLengthTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testBounds
{
    STAssertThrows( [[SVType string] minLength:@(-1)], nil);
    STAssertThrows( [[SVType string] maxLength:@(-10)], nil);
    
    STAssertNoThrow( [[[SVType string] minLength:@0] maxLength:@0], nil);
}

-(void)testMinMaxConflicts
{
    STAssertThrows( [[[SVType string] minLength:@(10)] maxLength:@(0)], nil);
    STAssertNoThrow( [[[SVType string] minLength:@(3)] maxLength:@(3)], nil);
}

-(void)testMaxMinConflicts
{
    STAssertThrows( [[[SVType string] maxLength:@(0)] minLength:@(10)], nil);
    STAssertNoThrow( [[[SVType string] maxLength:@(3)] minLength:@(3)], nil);
}

- (void)testEmptyString
{
    NSString* string = @"";
    NSError* error = nil;
    
    STAssertNotNil( [[[SVType string] minLength:@0] validateJson:string error:&error], nil);
    STAssertNil(error, nil);

    STAssertNotNil( [[[SVType string] maxLength:@0] validateJson:string error:&error], nil);
    STAssertNil(error, nil);

    error = nil;
    STAssertNil( [[[SVType string] minLength:@10] validateJson:string error:&error], nil);
    STAssertNotNil(error, nil);

    error = nil;
    STAssertNotNil( [[[SVType string] maxLength:@10] validateJson:string error:&error], nil);
    STAssertNil(error, nil);
}

- (void)testSimpleString
{
    NSString* string = @"abcde12345";
    NSError *error = nil;
    
    STAssertNotNil( [[[SVType string] minLength:@5] validateJson:string error:&error], nil);
    STAssertNil(error, nil);
    
    STAssertNotNil( [[[SVType string] maxLength:@20] validateJson:string error:&error], nil);
    STAssertNil(error, nil);
    
    error = nil;
    STAssertNil( [[[SVType string] maxLength:@5] validateJson:string error:&error], nil);
    STAssertNotNil(error, nil);

    error = nil;
    STAssertNil( [[[SVType string] minLength:@20] validateJson:string error:&error], nil);
    STAssertNotNil(error, nil);
}

@end
