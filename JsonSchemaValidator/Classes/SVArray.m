//
//  SVArray.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVArray.h"
#import "SVJsonSchema.h"
#import "NSObject+jsonTypes.h"
#import "NSString+errorMessages.h"

@implementation SVArray

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super initWithDictionary:dictionary];
    if ( self )
    {
        id minItems = dictionary[SVMinItemsKey];
        self.minItems = [minItems isJsonNumber] ? minItems : nil;
        
        id maxItems = dictionary[SVMaxItemsKey];
        self.maxItems = [maxItems isJsonNumber] ? maxItems : nil;
        
        id uniqueItems = dictionary[SVUniqueItemsKey];
        self.uniqueItems = [uniqueItems isJsonBoolean] ? [uniqueItems boolValue] : NO;
        
        id items = dictionary[SVItemsKey];
        
        if ( [items isJsonNull] )
        {
            [self items:nil];
        }
        else
        {
            if ( [items isJsonArray] )
            {
                NSMutableArray* schemas = [NSMutableArray array];
                for ( id item in items )
                {
                    id schema = [SVType schemaWithDictionary:item];
                    if ( schema )
                    {
                        [schemas addObject:schema];
                    }
                }
                self.items = schemas;
            }
            else if ( [items isJsonObject] )
            {
                self.items = [SVType schemaWithDictionary:items];
            }
        }
    }
    return self;
}

+(NSString *)type
{
    return SVArrayKey;
}

-(id)validateJson:( NSArray* )array errors:( NSMutableArray* )errors
{
    array = [super validateJson:array errors:errors];
    
    if ( !array )
    {
        return nil;
    }
    
    if ( array && ![array isKindOfClass:[NSArray class]] )
    {
        [errors addObject:[NSString mustBeType:[self.class type] instead:[array class]]];
        return nil;
    }
    
    // Invalidate the whole array if more items than a permitted maximum was provided
    if ( self.maxItems && array.count > [self.maxItems integerValue] )
    {
        [errors addObject:[NSString stringWithFormat:@"array must comtain maximum %d items", [self.minItems intValue]]];
        return nil;
    }
    
    if ( self.uniqueItems )
    {
        NSMutableSet* objects = [NSMutableSet setWithCapacity:array.count];
        __block NSString* uniqueItemsError = nil;
        [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop)
        {
            if ( [objects containsObject:obj] )
            {
                *stop = YES;
                uniqueItemsError = @"array must contain unique values";
            }
            [objects addObject:obj];
        }];
        if ( uniqueItemsError )
        {
            [errors addObject:uniqueItemsError];
            return nil;
        }
    }

    if ( !self.items || !array )
    {
        return array;
    }
    else
    {
        NSMutableArray* validatedArray = [NSMutableArray array];
        if ( [self.items isKindOfClass:[SVType class]])
        {
            [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop)
            {
                NSMutableArray* validatedErrors = [NSMutableArray array];
                id validated = [self.items validateJson:obj errors:validatedErrors];
                
                if ( validated )
                {
                    [validatedArray addObject:validated];
                }
             
                if ( validatedErrors.count )
                {
                    validatedErrors[0] = [NSString stringWithFormat:@"item %lu %@", (unsigned long)idx, validatedErrors[0]];
                    [errors addObjectsFromArray:validatedErrors];
                }
             
            }];

            if ( self.minItems && validatedArray.count < [self.minItems integerValue] )
            {
                [errors addObject:[NSString stringWithFormat:@"array must contain minimum %d valid items", [self.minItems intValue]]];
                return nil;
            }
            
            return validatedArray;
        }
        else if ( [self.items isKindOfClass:[NSArray class]] )
        {
            if ( [self.items count] < array.count )
            {
                [errors addObject:[NSString stringWithFormat:@"array must be at least %u long", (unsigned int)[self.items count]]];
                return nil;
            }
            
            NSMutableArray* validatedErrors = [NSMutableArray array];

            [self.items enumerateObjectsUsingBlock:^(id schema, NSUInteger idx, BOOL* stop)
            {
                id validated = [schema validateJson:array[idx] errors:validatedErrors];
                if ( validated )
                {
                    [validatedArray addObject:validated];
                }

                if ( validatedErrors.count )
                {
                    [errors addObjectsFromArray:validatedErrors];
                    *stop = YES;
                }
            }];
            
            return validatedErrors.count ? nil : validatedArray;
        }
        
        else {
            NSAssert(NO, @"items: must provide either an SVType or an array but the following was given: %@", self.items);
            return nil;
        }
    }
}

-(BOOL)isSchemaObjcClass:( SVObject* )schema
{
    return [schema respondsToSelector:@selector(objcClass)] && [schema performSelector:@selector(objcClass)];
}

-(id)instantiateValidatedJson:( NSArray* )validatedJsonArray
{
    if ( !self.items )
    {
        return validatedJsonArray;
    }
    
    if ( [self.items isJsonArray] )
    {
        NSMutableArray* instantiatedItems = [NSMutableArray arrayWithCapacity:[self.items count]];
        [(NSArray*)self.items enumerateObjectsUsingBlock:^(SVType* schema, NSUInteger idx, BOOL* stop)
        {
            id instance = [schema instantiateValidatedJson:validatedJsonArray[idx]];
            if ( instance )
                [instantiatedItems addObject:instance];
        }];
        return instantiatedItems;
    }
    else
    {
        if ( [self isSchemaObjcClass:self.items] )
        {
            NSMutableArray* instantiatedItems = [NSMutableArray arrayWithCapacity:validatedJsonArray.count];
            for ( id jsonObject in validatedJsonArray )
            {
                id instance = [self.items instantiateValidatedJson:jsonObject];
                if ( instance )
                    [instantiatedItems addObject:instance];
            }
            return instantiatedItems;
        }
        else
        {
            return validatedJsonArray;
        }
    }
}

-(void)setItems:( id )itemsOrNill
{
    if ( [_items isKindOfClass:[SVType class]] )
    {
        [_items setParent:nil];
    }
    else if ( [_items isKindOfClass:[NSArray class]] )
    {
        for ( SVType* type in _items)
            type.parent = nil;
    }

    if ( !itemsOrNill )
    {
        _items = itemsOrNill;
        return;
    }
    
    id items = nil;
    
    if ( [itemsOrNill isKindOfClass:[SVType class]] )
    {
        items = itemsOrNill;
        [itemsOrNill setParent:self];
    }
    
    static NSString* arrayException = @"JSON schema 'array'";
    
    if ( [itemsOrNill isKindOfClass:[NSArray class]] )
    {
        for ( SVType* type in itemsOrNill )
        {
            if ( ![type isKindOfClass:[SVType class]] )
            {
                [NSException raise:arrayException
                            format:@"property items must contain SVType or array of types, %@ instead", NSStringFromClass([type class])];
            }
            type.parent = self;
        }
        items = itemsOrNill;
    }
    
    if ( !items && itemsOrNill )
        [NSException raise:arrayException
                    format:@"unexpexted type if 'items' : %@", NSStringFromClass([itemsOrNill class])];
    
    _items = items;
}

-(instancetype)items:( id )itemsOrNill
{
    self.items = itemsOrNill;
    return self;
}

-(instancetype)minItems:( NSNumber* )minItems
{
    if ( self.maxItems && [self.maxItems compare:minItems] )
        [NSException raise:@"SVArray.minItems"
                    format:@"minItems:%@ must be less than maxItems:%@", minItems, self.maxItems];
    
    self.minItems = minItems;
    return self;
}

-(instancetype)maxItems:( NSNumber* )maxItems
{
    if ( self.minItems && [self.minItems compare:maxItems] == NSOrderedDescending )
        [NSException raise:@"SVArray.maxItems"
                    format:@"maxItems:%@ must be more than minItems:%@", maxItems, self.minItems];
    
    self.maxItems = maxItems;
    return self;
}

-(instancetype)uniqueItems:( BOOL )uniqueItems
{
    self.uniqueItems = uniqueItems;
    return self;
}

-(NSDictionary *)toJsonObject
{
    id items = [super toJsonObject];
    items[SVTypeKey] = SVArrayKey;
    
    if (self.items)
    {
        if ( [self.items isJsonArray] )
        {
            NSMutableArray* i = [NSMutableArray arrayWithCapacity:[self.items count]];
            for ( SVType* schema in self.items)
            {
                [i addObject:[schema toJsonObject]];
            }
            items[SVItemsKey] = i;
        }
        else
        {
            items[SVItemsKey] = [self.items toJsonObject];
        }
    }
    if (self.minItems) items[SVMinItemsKey] = self.minItems;
    if (self.maxItems) items[SVMaxItemsKey] = self.maxItems;
    if (self.maxItems) items[SVUniqueItemsKey] = @YES;
    
    return items;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"%@",[self toJsonObject]];
}

@end
