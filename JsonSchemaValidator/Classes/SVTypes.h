//
//  SVTypes.h
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const SVObjectKey;
extern NSString* const SVStringKey;
extern NSString* const SVIntegerKey;
extern NSString* const SVNumberKey;
extern NSString* const SVBooleanKey;
extern NSString* const SVArrayKey;
extern NSString* const SVNullKey;

extern NSString* const SVSchemaKey;
extern NSString* const SVTitleKey;
extern NSString* const SVIDKey;
extern NSString* const SVDescriptionKey;
extern NSString* const SVDefaultKey;
extern NSString* const SVTypeKey;
extern NSString* const SVMinimumKey;
extern NSString* const SVMaximumKey;
extern NSString* const SVExclusiveMinimumKey;
extern NSString* const SVExclusiveMaximumKey;
extern NSString* const SVMinItemsKey;
extern NSString* const SVMaxItemsKey;
extern NSString* const SVUniqueItemsKey;
extern NSString* const SVItemsKey;
extern NSString* const SVPropertiesKey;
extern NSString* const SVObjcClassKey;
extern NSString* const SVRequiredKey;
extern NSString* const SVEnumKey;

extern NSString* const SVFormatKey;
extern NSString* const SVURLKey;
extern NSString* const SVMaxLengthKey;
extern NSString* const SVMinLengthKey;
