//
//  SVTypes.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVTypes.h"

NSString* const SVObjectKey = @"object";
NSString* const SVStringKey = @"string";
NSString* const SVIntegerKey = @"integer";
NSString* const SVNumberKey = @"number";
NSString* const SVBooleanKey = @"boolean";
NSString* const SVArrayKey = @"array";
NSString* const SVNullKey = @"null";


NSString* const SVSchemaKey = @"$schema";
NSString* const SVTitleKey = @"title";
NSString* const SVIDKey = @"id";
NSString* const SVDescriptionKey = @"description";
NSString* const SVDefaultKey = @"default";
NSString* const SVTypeKey = @"type";
NSString* const SVRequiredKey = @"required";
NSString* const SVEnumKey = @"enum";
NSString* const SVMinimumKey = @"minimum";
NSString* const SVMaximumKey = @"maximum";
NSString* const SVExclusiveMinimumKey = @"exclusiveMminimum";
NSString* const SVExclusiveMaximumKey = @"exclusiveMaximum";
NSString* const SVMinItemsKey = @"minItems";
NSString* const SVMaxItemsKey = @"maxItems";
NSString* const SVUniqueItemsKey = @"uniqueItems";
NSString* const SVItemsKey = @"items";
NSString* const SVPropertiesKey = @"properties";
NSString* const SVObjcClassKey = @"#objcClass#";

NSString* const SVFormatKey = @"format";
NSString* const SVURLKey = @"uri";
NSString* const SVMaxLengthKey = @"maxLength";
NSString* const SVMinLengthKey = @"minLength";
