//
//  SVNumber.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVNumber.h"
#import "SVJsonSchema.h"
#import "NSObject+jsonTypes.h"
#import "NSString+errorMessages.h"

@implementation SVNumber

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super initWithDictionary:dictionary];
    
    if ( self )
    {
        id minimum = dictionary[SVMinimumKey];
        self.minimum = [minimum isJsonNumber] ? minimum : nil;
        
        id maximum = dictionary[SVMaximumKey];
        self.maximum = [maximum isJsonNumber] ? maximum : nil;
        
        id exclusiveMinimum = dictionary[SVExclusiveMinimumKey];
        self.exclusiveMinimum = [exclusiveMinimum isJsonBoolean] ? [exclusiveMinimum boolValue] : NO;
        
        id exclusiveMaximum = dictionary[SVExclusiveMaximumKey];
        self.exclusiveMaximum = [exclusiveMaximum isJsonBoolean] ? [exclusiveMaximum boolValue] : NO;
    }
    return self;
}

+(NSString *)type
{
    return SVNumberKey;
}

-(id)validateJson:( NSNumber* )number errors:(NSMutableArray *)errors
{
    number = [super validateJson:number errors:errors];

    if ( number && ![number isKindOfClass:[NSNumber class]] )
    {
        [errors addObject:[NSString mustBeType:[self.class type] instead:[number class]]];
        return nil;
    }
    
    if ( number && self.minimum )
    {
        NSComparisonResult compare = [number compare:self.minimum];
        
        if ( ( self.exclusiveMinimum && compare == NSOrderedSame ) || compare == NSOrderedAscending )
        {
            [errors addObject:[NSString stringWithFormat:@"%@ must be more %@ to %@", number, self.exclusiveMinimum? @"than": @"or equal to", self.minimum]];
            return nil;
        }
    }
    
    if( number && self.maximum )
    {
        NSComparisonResult compare = [number compare:self.maximum];
        
        if ( ( self.exclusiveMaximum && compare == NSOrderedSame ) || compare == NSOrderedDescending )
        {
            [errors addObject:[NSString stringWithFormat:@"%@ must be less %@ %@", number, self.exclusiveMinimum? @"than": @"or equal to", self.maximum]];
            return nil;
        }
    }

    return number;
}

-(instancetype)exclusiveMinimum:(BOOL)exclusiveMinimum
{
    self.exclusiveMinimum = exclusiveMinimum;
    return self;
}

-(instancetype)exclusiveMaximum:(BOOL)exclusiveMaximum
{
    self.exclusiveMaximum = exclusiveMaximum;
    return self;
}

-(instancetype)minimum:( NSNumber* )minimum
{
    if ( self.maximum && [self.maximum compare:minimum] == NSOrderedAscending )
        [NSException raise:@"SVNumber.minimum"
                    format:@"minimum:%@ must be less than maximum:%@", minimum, self.maximum];
    
    self.minimum = minimum;
    return self;
}

-(instancetype)maximum:( NSNumber* )maximum
{
    if ( self.minimum && [self.minimum compare:maximum] == NSOrderedDescending )
        [NSException raise:@"SVNumber.maximum"
                    format:@"maximum:%@ must be more than minimum:%@", maximum, self.minimum];
    
    self.maximum = maximum;
    return self;
}

-(NSDictionary *)toJsonObject
{
    id items = [super toJsonObject];
    items[SVTypeKey] = SVNumberKey;
    if ( self.minimum ) items[SVMinimumKey] = self.minimum;
    if ( self.maximum ) items[SVMaximumKey] = self.maximum;
    if ( self.exclusiveMinimum ) items[SVExclusiveMinimumKey] = @YES;
    if ( self.exclusiveMaximum ) items[SVExclusiveMaximumKey] = @YES;
    return items;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"%@",[self toJsonObject]];
}

@end
