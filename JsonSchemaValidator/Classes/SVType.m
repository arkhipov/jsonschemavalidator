//
//  SVType.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVType.h"

#import "SVTypes.h"
#import "SVJsonSchema.h"

@interface SVType ()

@property (strong, nonatomic) NSNumber* requiredBoolean;

@end

@implementation SVType

@dynamic required;

+(id)schemaWithDictionary:( NSDictionary* )dictionary
{
    NSDictionary* types = @{
                            SVArrayKey : [SVArray class],
                            SVObjectKey : [SVObject class],
                            SVStringKey : [SVString class],
                            SVIntegerKey : [SVInteger class],
                            SVNumberKey : [SVNumber class],
                            SVNullKey : [SVNull class],
                            SVBooleanKey : [SVBoolean class]
    };

    NSString* type  = dictionary[SVTypeKey];
    if ( !type )
        return nil;
    
    Class typeClass = types[type];
    return typeClass ? [[typeClass alloc] initWithDictionary:dictionary] : nil;
}

-(id)initWithDictionary:( NSDictionary* )dictionary
{
    self = [super init];
    if ( self )
    {
        self.schema = dictionary[SVSchemaKey];
        self.schemaId = dictionary[SVIDKey];
        self.title =  dictionary[SVTitleKey];
        self.schemaDescription = dictionary[SVDescriptionKey];
        self.defaultVal = dictionary[SVDefaultKey];
        
        id required = dictionary[SVRequiredKey];
        self.required = [required isJsonNumber] ? [required boolValue] : NO;
        
        id enumValues = dictionary[SVEnumKey];
        self.enumValues = [enumValues isJsonArray] ? enumValues : nil;
    }
    return self;
}

+(NSString *)type
{
    [[self new] doesNotRecognizeSelector:_cmd];
    return nil;
}

-(NSString *)type
{
    return [[self class] type];
}

-(instancetype)schemaId:( NSString* )newId
{
    self.schemaId = newId;
    return self;
}

-(instancetype)schema:( NSString* )schema
{
    self.schema = schema;
    return self;
}

-(instancetype)title:( NSString* )title
{
    self.title = title;
    return self;
}

-(instancetype)description:( NSString* )description;
{
    self.schemaDescription = description;
    return self;
}

-(instancetype)defaultVal:(id)value
{
    self.defaultVal = value;
    return self;
}

-(instancetype)required:(BOOL)required
{
    self.required = required;
    return self;
}

-(void)setRequired:(BOOL)required
{
    self.requiredBoolean = @(required);
}

-(BOOL)required
{
    if ( !_requiredBoolean )
        return NO;
    
    return [self.requiredBoolean boolValue];
}

-(BOOL)assertEnumValuesAreValid:(NSArray *)enumValues
{
    NSMutableArray* errors = [NSMutableArray array];
    for ( id type in enumValues)
    {
        [self validateJson:type errors:errors];
    }
    
    if ( errors.count )
    {
        [NSException raise:@"enumValues" format:@"%@", [errors componentsJoinedByString:@", "]];
        return NO;
    }
    else {
        return YES;
    }
}

-(instancetype)enumValues:(NSArray *)enumValues
{
    if ( ![self assertEnumValuesAreValid:enumValues] ) { return nil; }

    self.enumValues = enumValues;
    return self;
}

+(SVObject*)object
{
    return [SVObject new];
}

+(SVString*)string
{
    return [SVString new];
}

+(SVInteger*)integer
{
    return [SVInteger new];
}

+(SVNumber*)number
{
    return [SVNumber new];
}

+(SVBoolean*)boolean
{
    return [SVBoolean new];
}

+(SVArray*)array
{
    return [SVArray new];
}

+(SVNull*)null
{
    return [SVNull new];
}

-(id)matchEnumValues:( id )jsonObject errors:( NSMutableArray* )errors
{
    if ( self.enumValues && ![self.enumValues containsObject:jsonObject] )
    {
        [errors addObject:[NSString stringWithFormat:@"must be one of [%@], but %@", [self.enumValues componentsJoinedByString:@", "], jsonObject]];
        return nil;
    }
    return jsonObject;
}

-(id)validateJson:( id )jsonObject errors:( NSMutableArray* )errors
{
    if ( !jsonObject )
    {
        jsonObject = self.defaultVal;
    }
    
    if ( !jsonObject && self.required )
    {
        [errors addObject:@"is required"];
        return nil;
    }
    
    jsonObject = [self matchEnumValues:jsonObject errors:errors];

    return jsonObject;
}

+(SVObject*)draft;
{
    NSString* path = [[NSBundle bundleForClass:[self class]] pathForResource:@"json-schema.draft-04.schema"
                                                                      ofType:@"json"];
    
    NSData* draftData = [NSData dataWithContentsOfFile:path];
    return [SVType schemaWithDictionary:[NSJSONSerialization JSONObjectWithData:draftData options:0 error:NULL]];
}

-(id)validateJson:(id)jsonObject error:( NSError* __autoreleasing* )error
{
    NSMutableArray* errors = [NSMutableArray array];
    id validated =  [self validateJson:jsonObject errors:errors];
    if ( errors.count != 0 && !*error )
    {
        *error = [NSError errorWithDomain:@"JSON validation"
                                     code:0
                                 userInfo:@{NSLocalizedDescriptionKey:[errors componentsJoinedByString:@", "]}];
    }
    return validated;

}

-(id)validateAndInstanciateJson:( id )jsonObject error:( NSError *__autoreleasing * )error
{
    id validated = [self validateJson:jsonObject error:error];
    return [self instantiateValidatedJson:validated];
}

-(id)instantiateValidatedJson:( NSDictionary* )validatedJson
{
    return validatedJson;
}

-(NSMutableDictionary*)toJsonObject
{
    NSMutableDictionary* items = [NSMutableDictionary dictionary];
    if (self.schema) items[SVSchemaKey] = self.schema;
    if (self.schemaId)  items[SVIDKey] = self.schemaId;
    if (self.title) items[SVTitleKey] = self.title;
    if (self.schemaDescription) items[SVDescriptionKey] = self.schemaDescription;
    if (self.defaultVal) items[SVDefaultKey] = self.defaultVal;
    if (self.required) items[@"required"] = @YES;
    if (self.enumValues) items[@"enum"] = self.enumValues;
    return items;
}

@end
