//
//  SVType.h
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SVAny;
@class SVObject;
@class SVString;
@class SVInteger;
@class SVNumber;
@class SVBoolean;
@class SVArray;
@class SVNull;

@interface SVType : NSObject

@property (weak, nonatomic) SVType* parent;
@property (strong, nonatomic) NSString* schema;
@property (strong, nonatomic) NSString* schemaId;
@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSString* schemaDescription;
@property (strong, nonatomic) id defaultVal;
@property (assign, nonatomic) BOOL required;
@property (strong, nonatomic) NSArray* enumValues;

+(id)schemaWithDictionary:( NSDictionary* )dictionary;
-(instancetype)initWithDictionary:( NSDictionary* )dictionary;

-(instancetype)schemaId:( NSString* )newId;
-(instancetype)schema:( NSString* )schema;
-(instancetype)title:( NSString* )title;
-(instancetype)description:( NSString* )description;
-(instancetype)defaultVal:( id )value;
-(instancetype)required:( BOOL )required;
-(instancetype)enumValues:( NSArray* )enumValues;

+(SVArray*)array;
+(SVBoolean*)boolean;
+(SVInteger*)integer;
+(SVNumber*)number;
+(SVNull*)null;
+(SVObject*)object;
+(SVString*)string;

+(SVObject*)draft;

+(NSString*)type;
-(NSString*)type;

-(id)validateJson:( id )jsonObject errors:( NSMutableArray* )errors;
-(id)validateJson:( id )jsonObject error:( NSError* __autoreleasing* )error;

-(id)validateAndInstanciateJson:( id )jsonObject error:( NSError* __autoreleasing* )error;

-(id)instantiateValidatedJson:( NSDictionary* )validatedJson;

-(NSMutableDictionary*)toJsonObject;

@end
