//
//  SVNumber.h
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVType.h"

@interface SVNumber : SVType

@property (strong, nonatomic) NSNumber* minimum;
@property (strong, nonatomic) NSNumber* maximum;
@property (assign, nonatomic) BOOL exclusiveMinimum;
@property (assign, nonatomic) BOOL exclusiveMaximum;

-(instancetype)minimum:( NSNumber* )min;
-(instancetype)maximum:( NSNumber* )max;
-(instancetype)exclusiveMinimum:( BOOL )exclusiveMinimum;
-(instancetype)exclusiveMaximum:( BOOL )exclusiveMaximum;

@end
