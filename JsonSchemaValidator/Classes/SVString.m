//
//  SVString.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVString.h"
#import "SVJsonSchema.h"
#import "NSString+errorMessages.h"

#define URI_REGEX_PATTERN @"([^:/?#]+)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
static NSRegularExpression *__uriRegex = nil;

@implementation SVString

+ (void)load
{
    NSError *error = nil;
    __uriRegex = [NSRegularExpression regularExpressionWithPattern:URI_REGEX_PATTERN
                                                           options:NSRegularExpressionCaseInsensitive
                                                             error:&error];
    NSAssert(!error, @"Failed to create URI regex: %@", error);
}


+(NSString *)type
{
    return SVStringKey;
}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super initWithDictionary:dictionary];
    if ( self )
    {
        self.format = dictionary[SVFormatKey];
        self.maxLength = dictionary[SVMaxLengthKey];
        self.minLength = dictionary[SVMinLengthKey];
    }
    return self;
}

-(id)validateJson:( NSString* )mustBeString errors:( NSMutableArray* )errors
{
    id validated = [super validateJson:mustBeString errors:errors];

    if ( validated && ![validated isKindOfClass:[NSString class]] )
    {
        [errors addObject:[NSString mustBeType:[self.class type] instead:[mustBeString class]]];
        return nil;
    }
    
    if ( validated && self.minLength && ([mustBeString length] < [self.minLength unsignedIntegerValue]) ) {
        [errors addObject:[NSString stringWithFormat:@"String '%@' must be at least %@ characters long",
                           mustBeString, self.minLength]];
        return nil;
    }
    
    if ( validated && self.maxLength && ([mustBeString length] > [self.maxLength unsignedIntegerValue]) ) {
        [errors addObject:[NSString stringWithFormat:@"String '%@' must be at most %@ characters long",
                           mustBeString, self.maxLength]];
        return nil;
    }
    
    if ( mustBeString && validated && [self.format isEqualToString:SVURLKey] ) {
        NSTextCheckingResult *firstMatch = [__uriRegex firstMatchInString:mustBeString
                                                                  options:0
                                                                    range:NSMakeRange(0, [mustBeString length])];
        if (!firstMatch || firstMatch.range.location == NSNotFound) {
            [errors addObject:[NSString stringWithFormat:@"String '%@' must have URI format", mustBeString]];
            return nil;
        }
    }
    
    return validated;
}

-(NSMutableDictionary *)toJsonObject
{
    id items = [super toJsonObject];
    items[SVTypeKey] = SVStringKey;
    
    if (self.format)
    {
        items[SVFormatKey] = self.format;
    }
    
    if (self.minLength)
    {
        items[SVMinLengthKey] = self.minLength;
    }
    
    if (self.maxLength)
    {
        items[SVMaxLengthKey] = self.maxLength;
    }
    
    return items;
}

-(id)instantiateValidatedJson:( NSString* )validatedJson
{
    if ( [self.format isEqualToString:SVURLKey] )
    {
        return [NSURL URLWithString:validatedJson];
    }
    return validatedJson;
}

-(instancetype)format:( NSString* )format
{
    self.format = format;
    return self;
}

-(instancetype)minLength:( NSNumber* )minLength
{
    NSAssert([minLength compare:@0] != NSOrderedAscending,
             @"minLength (given %@) must be greater than or equal to 0.", minLength);
    if (self.maxLength) {
        NSAssert([minLength compare:self.maxLength] != NSOrderedDescending,
                 @"minLength (given %@) must not be greater than maxLength (currently %@).",
                 minLength, self.maxLength);
    }
    self.minLength = minLength;
    return self;
}

-(instancetype)maxLength:( NSNumber* )maxLength
{
    NSAssert([maxLength compare:@0] != NSOrderedAscending,
             @"maxLength (given %@) must be greater than or equal to 0.", maxLength);
    if (self.minLength) {
        NSAssert([self.minLength compare:maxLength] != NSOrderedDescending,
                 @"maxLength (given %@) must not be less than minLength (currently %@).",
                 maxLength, self.minLength);
    }
    self.maxLength = maxLength;
    return self;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"<%@ %@>", NSStringFromClass([self class]), [self toJsonObject]];
}

@end
