//
//  SVBoolean.h
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVType.h"

@interface SVBoolean : SVType

@end
