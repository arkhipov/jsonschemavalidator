//
//  SVString.h
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVType.h"

@interface SVString : SVType

@property (strong, nonatomic) NSString* format;
@property (strong, nonatomic) NSNumber* minLength;
@property (strong, nonatomic) NSNumber* maxLength;

-(instancetype)format:( NSString* )format;
-(instancetype)minLength:( NSNumber* )minLength;
-(instancetype)maxLength:( NSNumber* )maxLength;

@end
