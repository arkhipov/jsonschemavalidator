//
//  NSObject+jsonTypes.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/23/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "NSObject+jsonTypes.h"
#import "SVJsonSchema.h"
#import <objc/runtime.h>

@implementation NSObject (jsonTypes)

-(BOOL)isJsonInteger
{
    return NO;
}

-(BOOL)isJsonNumber
{
    return NO;
}

-(BOOL)isJsonBoolean
{
    return NO;
}

-(BOOL)isJsonNull
{
    return NO;
}

-(BOOL)isJsonArray
{
    return NO;
}

-(BOOL)isJsonObject
{
    return NO;
}

-(BOOL)isJsonString
{
    return NO;
}

+(SVType*)jsonSchemaForProperty:(NSString*)propertyName
{
    objc_property_t property = class_getProperty( self, [propertyName UTF8String] );
	if ( property == NULL )
		return ( NULL );
    
    NSString* propertyTypeAttributes = [NSString stringWithUTF8String:property_getAttributes( property )];
    NSArray * attributes = [propertyTypeAttributes componentsSeparatedByString:@","];
    
    NSString* type = attributes[0];
    
    if ( type.length >= 2 && [type characterAtIndex:0] == 'T' && [type characterAtIndex:1] == '@' ) // piinter to id or object
    {
        NSString* classString = [[type stringByReplacingOccurrencesOfString:@"T@"
                                                                 withString:@""]
                                       stringByReplacingOccurrencesOfString:@"\""
                                                                 withString:@""];
        
        Class class = NSClassFromString(classString);
        
        if ( !class || class == [NSObject class]  )
            return nil;
        
        SVType* schema = [class jsonSchema];
        [self prepareJsonSchema:schema forProperty:propertyName];
        return schema;
    }

    return nil;
}

+(void)prepareJsonSchema:( SVType* )schema forProperty:( NSString* )property { }

+(SVType*)jsonSchema
{
    SVObject* objectSchema = [SVType object];
    
    NSMutableDictionary* properties = [NSMutableDictionary dictionary];
    for (NSString* property in [self propertyNames])
    {
        id propertySchema = [self jsonSchemaForProperty:property];
        if ( propertySchema )
            properties[property] = propertySchema;
    }
    objectSchema.properties = properties;
    objectSchema.objcClass = [self class];
    
    return objectSchema;
}

+(NSArray*)propertyNames
{
	unsigned int i, count = 0;
	objc_property_t * properties = class_copyPropertyList( self, &count );

	if ( count == 0 )
	{
		free( properties );
		return ( nil );
	}

	NSMutableArray * list = [NSMutableArray arrayWithCapacity:count];
    
	for ( i = 0; i < count; i++ )
		[list addObject:[NSString stringWithUTF8String:property_getName(properties[i])]];
    
    free( properties );
    
	return ( [list copy] );
}

-(SVType*)jsonSchema
{
    return [self.class jsonSchema];
}

-(id)initWithJson:( NSDictionary* )json schema:( SVObject* )objectSchema
{
    self = [self init];
    if ( self )
    {
        [objectSchema.properties enumerateKeysAndObjectsUsingBlock:^(id key, SVType* schema, BOOL* stop)
        {
            id value = json[key];
            [self setValue:[schema instantiateValidatedJson:value] forKey:key];
        }];
    }
    return self;
}

+(id)instanceFromJson:( NSDictionary* )json schema:( SVType* )schema
{
    return [[self alloc] initWithJson:json schema:schema];
}

+(id)validateJson:( id )jsonObject schema:( SVObject* )schema errors:( NSMutableArray* )errors
{
    return [schema validateJsonObject:jsonObject errors:errors];
}

@end


@implementation NSNull (jsonTypes)

-(BOOL)isJsonNull
{
    return YES;
}

+(SVType *)jsonSchema
{
    return [SVType null];
}

-(SVType *)jsonSchema
{
    return [self.class jsonSchema];
}

@end

@implementation NSString (jsonTypes)

-(BOOL)isJsonString
{
    return YES;
}

+(SVType *)jsonSchema
{
    return [SVType string];
}

-(SVType *)jsonSchema
{
    return [self.class jsonSchema];
}

@end

